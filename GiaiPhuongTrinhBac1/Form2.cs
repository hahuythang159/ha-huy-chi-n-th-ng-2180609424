﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GiaiPhuongTrinhBac1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void btnGiai(object sender, EventArgs e)
        {
            int heSoA=Int32.Parse(txtHeSoA.Text);
            int heSoB = Int32.Parse(txtHeSoB.Text);
            if (heSoA == 0 && heSoB == 0)
                txtKetQua.Text = "Phuong trinh co vo so nghiem";
            else if (heSoA == 0 && heSoB != 0)
                txtKetQua.Text = "Phuong trinh vo nghiem";
            else if (heSoA != 0)
            {
                txtKetQua.Text = "Phuong tirnh co 1 nghiem duy nhat";
                txtNghiem.Text = (-heSoB/heSoA).ToString();
            }
        }

        private void btnTiep(object sender, EventArgs e)
        {
            txtHeSoA.Clear();
            txtHeSoB.Text = string.Empty;
            txtKetQua.Text = "";
            txtNghiem.Clear();

        }

        private void btnThoat(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_MouseCaptureChanged(object sender, EventArgs e)
        {

        }

        private void txtKetQua1(object sender, EventArgs e)
        {

        }

        private void txt(object sender, EventArgs e)
        {

        }

        private void txt1(object sender, EventArgs e)
        {

        }

        private void txtNghiem1(object sender, EventArgs e)
        {

        }
    }
}
